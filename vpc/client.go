package vpc

import (
	"bitbucket.org/mathildetech/qcloud-sdk-go/common"
)

const (
	// CLBDefaultEndpoint is the default API endpoint of CVM services
	VPCDefaultEndpoint = "https://vpc.api.qcloud.com/v2/index.php"
	VPCAPIVersion      = "2016-10-28"
)

// A Client represents a client of VPC
type Client struct {
	*common.Client
}

// NewClient creates a new instance of VPC client
func NewClient(credential common.Credential, opts common.Opts) (*Client, error) {
	if opts.Endpoint == "" {
		opts.Endpoint = VPCDefaultEndpoint
	}
	if opts.Version == "" {
		opts.Version = VPCAPIVersion
	}
	client, err := common.NewClient(credential, opts)
	if err != nil {
		return &Client{}, err
	}
	return &Client{client}, nil
}
