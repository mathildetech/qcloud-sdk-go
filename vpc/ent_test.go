package vpc

import (
	"bitbucket.org/mathildetech/qcloud-sdk-go/common"
	"flag"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"time"
)

var (
	client *Client
)

func init() {
	credential := common.Credential{
		SecretId:  "AKID9Wsy27Rvd2fypNpl7irlPh1IoGGv83oE",
		SecretKey: "mQmW3k0cSAKWjg4Yeq9wNYYE9OXwS6Y9",
	}
	opts := common.Opts{
		Region: "ap-beijing",
		Debug:  true,
	}
	client, _ = NewClient(credential, opts)
}

func TestVpc(t *testing.T) {
	assert := assert.New(t)

	// describe nics
	nics, err := client.DescribeNetworkInterfaces(&DescribeNetworkInterfacesArgs{})
	assert.NoError(err)
	assert.NotEmpty(nics)

	vpcId := nics[0].VpcId
	nicId := nics[0].NetworkInterfaceId

	//assign private ip address
	res, err := client.AssignPrivateIpAddress(&AssignPrivateIpAddressArgs{
		VpcId:              vpcId,
		NetworkInterfaceId: nicId,
		PrivateIpAddress:   "10.110.110.199"})
	assert.NoError(err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	time.Sleep(2 * time.Second)
	oldNicNum := len(nics[0].PrivateIpAddressesSet)
	nics, err = client.DescribeNetworkInterfaces(&DescribeNetworkInterfacesArgs{NetworkInterfaceId: nicId})
	assert.NoError(err)
	assert.NotEmpty(nics)
	assert.Equal(oldNicNum+1, len(nics[0].PrivateIpAddressesSet))

	// unassign private ip address
	res, err = client.UnassignPrivateIpAddress(&UnassignPrivateIpAddressArgs{
		VpcId:              vpcId,
		NetworkInterfaceId: nicId,
		PrivateIpAddress:   "10.110.110.199"})
	assert.NoError(err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	time.Sleep(2 * time.Second)
	nics, err = client.DescribeNetworkInterfaces(&DescribeNetworkInterfacesArgs{NetworkInterfaceId: nicId})
	assert.NoError(err)
	assert.NotEmpty(nics)
	assert.Equal(oldNicNum, len(nics[0].PrivateIpAddressesSet))

	// address migration
	nics, err = client.DescribeNetworkInterfaces(&DescribeNetworkInterfacesArgs{})
	assert.NoError(err)
	assert.NotEmpty(nics)

	res, err = client.AssignPrivateIpAddress(&AssignPrivateIpAddressArgs{
		VpcId:              nics[0].VpcId,
		NetworkInterfaceId: nics[0].NetworkInterfaceId,
		PrivateIpAddress:   "10.110.110.199"})
	assert.NoError(err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	res, err = client.AssignPrivateIpAddress(&AssignPrivateIpAddressArgs{
		VpcId:              nics[1].VpcId,
		NetworkInterfaceId: nics[1].NetworkInterfaceId,
		PrivateIpAddress:   "10.110.110.200"})
	assert.NoError(err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	time.Sleep(2 * time.Second)
	res, err = client.MigratePrivateIpAddress(&MigratePrivateIpAddressArgs{
		VpcId:                 nics[0].VpcId,
		PrivateIpAddress:      "10.110.110.200",
		OldNetworkInterfaceId: nics[1].NetworkInterfaceId,
		NewNetworkInterfaceId: nics[0].NetworkInterfaceId})
	assert.NoError(err, err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	time.Sleep(2 * time.Second)
	nics, err = client.DescribeNetworkInterfaces(&DescribeNetworkInterfacesArgs{NetworkInterfaceId: nics[0].NetworkInterfaceId})
	assert.NoError(err, err)
	assert.Equal(3, len(nics[0].PrivateIpAddressesSet))

	time.Sleep(2 * time.Second)
	res, err = client.UnassignPrivateIpAddress(&UnassignPrivateIpAddressArgs{
		VpcId:              nics[0].VpcId,
		NetworkInterfaceId: nics[0].NetworkInterfaceId,
		PrivateIpAddress:   "10.110.110.199"})
	assert.NoError(err, err)
	assert.Equal(res.Code, 0, res.CodeDesc)

	time.Sleep(2 * time.Second)
	res, err = client.UnassignPrivateIpAddress(&UnassignPrivateIpAddressArgs{
		VpcId:              nics[0].VpcId,
		NetworkInterfaceId: nics[0].NetworkInterfaceId,
		PrivateIpAddress:   "10.110.110.200"})
	assert.NoError(err, err)
	assert.Equal(res.Code, 0, res.CodeDesc)
}

func TestMain(m *testing.M) {
	flag.Set("alsologtostderr", "true")
	flag.Parse()
	code := m.Run()
	os.Exit(code)
}
