# qcloud-sdk-go

## Usage
#  govendor
```
govendor fetch bitbucket.org/mathildetech/qcloud-sdk-go/common
govendor fetch bitbucket.org/mathildetech/qcloud-sdk-go/cvm
govendor fetch bitbucket.org/mathildetech/qcloud-sdk-go/util
```

```go
package main

import (
	"log"

	"bitbucket.org/mathildetech/qcloud-sdk-go/cvm"
	"bitbucket.org/mathildetech/qcloud-sdk-go/common"
)

func main() {
	credential := common.Credential{
		SecretId: "YOUR_SECRET_ID",
		SecretKey: "YOUR_SECRET_KEY",
	}
	opts := common.Opts{
		Region: "gz",
	}
	client, err := cvm.NewClient(credential, opts)
	if err != nil {
		log.Fatal(err)
	}
	args := cvm.DescribeInstances{}

	cmvs, err := client.DescribeLoadBalancers(&args)
	if err != nil {
		log.Fatal(lbs)
	}
	log.Println(cmvs.InstanceSet)
}



```